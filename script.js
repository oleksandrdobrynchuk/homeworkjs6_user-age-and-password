//     1. Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування.
//          Екранування здійснюється зворотним слешем та використовується для позначення класів символів, наприклад d.
//          Це спеціальний символ у регулярних виразах. Також для виведення в кавичках кавичок та інших символів.

//     2. Які засоби оголошення функцій ви знаєте?
//          Function expression, function declaration, array function.

//     3. Що таке hoisting, як він працює для змінних та функцій?
//          Для function declaration hoisting працює при оголошенні функції в будь-якому місці коду.
//          Для Function expression та змінних потрібно спочатку оголосити функції, змінну і вже тоді до них звертатися.

//Завдання

let inputFirstName = document.querySelector('.input-first-name');
let inputLastName = document.querySelector('.input-last-name');
let inputDate = document.querySelector('.input-date');

function createNewUser (firstName = 'title', lastName = 'title', date = 'title') {

    const newUser = {
        _firstName: firstName,
        _lastName: lastName,
        _birthDate: new Date(date),

        getLogin () {
            return (this._firstName.slice(0, 1) + this._lastName).toLowerCase();
        },

        getAge () {
            const nowDate = new Date();
            const nowYear = nowDate.getFullYear();
            const birthDateYear = this._birthDate.getFullYear();
            return nowYear - birthDateYear;
        },

        getPassword () {
            const birthDateYear = this._birthDate.getFullYear();
            return this._firstName[0].toUpperCase() + this._lastName.toLowerCase() + birthDateYear;
        },
    };
    return newUser;
}

document.querySelector(".button").addEventListener("click", () => {
    const user = createNewUser(inputFirstName.value, inputLastName.value, inputDate.value)
    document.querySelector(".output").innerText = "Login: " + user.getLogin() + " Age: " +user.getAge() + " Password: " + user.getPassword();
    console.log("Login: " + user.getLogin() + " Age: " +user.getAge() + " Password: " + user.getPassword());
});


